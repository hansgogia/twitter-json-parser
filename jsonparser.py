import simplejson
import urllib2
import pprint
from pyh import *
req = urllib2.Request("https://api.twitter.com/1/statuses/user_timeline.json?id=hansg01")
opener = urllib2.build_opener()
f = opener.open(req)
a = simplejson.load(f)
page = PyH("Twitter JSON parsing")
page << h1('Python Twitter JSON parsing by hansg01', cl='center')
page << div(id = 'maindiv')
l=[]
b=0s
for i in range(len(a)):
	l.append(a[i]['text'])
	l.append(a[i]['created_at'])
	l.append(a[i]['source'])
	l.append(a[i]['in_reply_to_screen_name'])
	page.maindiv <<p(l[i])
page.printOut()